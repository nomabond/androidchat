package com.firebase.androidchat;

import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.snapshot.ChildKey;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class MainActivity extends ActionBarActivity {

    private ArrayList<String> names;
    private String mUsername;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    private ChatListAdapter mChatListAdapter;

    private Random rng;
    // For rapid prototyping use these to generate a roomID
    private final String ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private final int ROOM_ID_LENGTH = 5;

    private String roomID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        rng = new Random();
        // Make sure we have a mUsername
        setupUsername();

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if(extras == null) {
                roomID = null;
            } else {
                roomID = extras.getString("roomID");
            }
        } else {
            roomID = (String) savedInstanceState.getSerializable("roomID");
        }

        // Setup our Firebase mFirebaseRef
        if(roomID == null) {
            roomID = (generateRoomID(rng, ALPHA, ROOM_ID_LENGTH));
        }

        mFirebaseRef = new Firebase(getString(R.string.FIREBASE_URL)).child(roomID);


        // Setup our input methods. Enter key on the keyboard or pushing the send button
        EditText inputText = (EditText) findViewById(R.id.messageInput);
        inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });

        findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });

        inputText.setOnKeyListener(new View.OnKeyListener()
        {
            public boolean onKey(View v, int keyCode, KeyEvent event)
            {
                if (event.getAction() == KeyEvent.ACTION_DOWN)
                {
                    switch (keyCode)
                    {
                        case KeyEvent.KEYCODE_DPAD_CENTER:
                        case KeyEvent.KEYCODE_ENTER:
                            sendMessage();
                            return true;
                        default:
                            break;
                    }
                }
                return false;
            }
        });

    }

    public static String generateRoomID(Random rng, String characters, int length)
    {
        char[] text = new char[length];
        for (int i = 0; i < length; i++)
        {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

    @Override
    public void onStart() {
        super.onStart();
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        final ListView listView = (ListView) findViewById(R.id.chatList);
        // Tell our list adapter that we only want 50 messages at a time
        mChatListAdapter = new ChatListAdapter(mFirebaseRef.limitToFirst(50), this, R.layout.chat_message, mUsername);
        listView.setAdapter(mChatListAdapter);
        mChatListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(mChatListAdapter.getCount() - 1);
            }
        });

        setTitle("Anonymeeting: " + roomID);

        // Finally, a little indication of connection status
        mConnectedListener = mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    systemBroadcast("has joined " + roomID + ".");
                } else {
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        mFirebaseRef.getRoot().child("chat").child(roomID).setValue(null);
        mFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mChatListAdapter.cleanup();
    }

    private void setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences("ChatPrefs", 0);
        mUsername = prefs.getString("username", null);
        mUsername = null;
        names = new ArrayList<>(Arrays.asList(new String[]{"tryhard","pitchfork","vhs","banh","mi","before","they","sold","out","typewriter","williamsburg","brooklyn","synth","pug","squid","bespoke","synth","farmtotable","authentic","echo","park","chillwave","thundercats","meggings","fingerstache","+1","selvage","gochujang","austin","kinfolk","williamsburg","fap","thundercats","diy","letterpress","affogato","forage","coldpressed","biodiesel","squid","mlkshk","sustainable","farmtotable","chicharrones","pork","belly","chambray","ennui","narwhal","vice","small","batch","taxidermy","kale","chips","tattooed","pinterest","gochujang","bitters","poutine","wayfarers","forage","brooklyn","mumblecore","vinyl","keytar","crucifix","trust","fund","aesthetic","kombucha","beard","venmo","affogato","fanny","pack","shabby","chic","art","party","tryhard","bicycle","rights","humblebrag","schlitz","put","a","bird","on","it","blue","bottle","direct","trade","selfies","plaid","migas","food","truck","fingerstache","slowcarb","meggings","vhs","green","juice","viral","distillery","stumptown","man","bun","gastropub","cronut","salvia","slowcarb","retro","organic","kinfolk","normcore","polaroid","forage","leggings","brooklyn","pabst","pinterest","neutra","viral","food","truck","cred","leggings","locavore","neutra","vice","disrupt","waistcoat","thundercats","tryhard","irony","coldpressed","migas","lumbersexual","actually","gastropub","cray","fingerstache","yr","cred","neutra","umami","tofu","microdosing","offal","ramps","jean","shorts","letterpress","organic","swag","put","a","bird","on","it","seitan","vinyl","lofi","kickstarter","polaroid","cray","leggings","slowcarb","flexitarian","meditation","stumptown","jean","shorts","fixie","3","wolf","moon","readymade","schlitz","occupy","art","party","vinyl","man","bun","narwhal","pourover","gastropub","plaid","quinoa","tousled","bitters","3","wolf","moon","man","braid","photo","booth","sriracha","migas","flexitarian","disrupt","wolf","affogato","next","level","hammock","leggings","stumptown","mlkshk","+1","yolo","poutine","kogi","shoreditch","vhs","waistcoat","paleo","shabby","chic","popup","kitsch","meditation","small","batch","franzen","leggings","brooklyn","viral","intelligentsia","trust","fund","normcore","yr","synth","godard","vinyl","you","probably","havent","heard","of","them","beard","craft","beer","tousled","marfa","fanny","pack","singleorigin","coffee","venmo","direct","trade","vinyl","","disrupt","drinking","vinegar","vice","iphone","sartorial","freegan","letterpress","viral","tryhard","squid","listicle","cronut","health","goth","fanny","pack","freegan","small","batch","portland","vinyl","xoxo","semiotics","vice","sustainable","3","wolf","moon","selfies","godard","scenester","intelligentsia","vice","organic","glutenfree","heirloom","pourover","yolo","wayfarers","paleo","sartorial","hashtag","tumblr","street","art","brooklyn","lumbersexual","intelligentsia","yuccie","franzen","shabby","chic","occupy","mustache","leggings","paleo","hammock","helvetica","vinyl","coldpressed","lomo","mumblecore","aesthetic","williamsburg","gentrify","mlkshk","green","juice","schlitz","tousled","pbr&b","tilde","gastropub","brunch","fingerstache","art","party","mustache","plaid","listicle","sartorial","singleorigin","coffee","swag","freegan","yr","street","art","polaroid","fashion","axe","synth","artisan"}));
        if (mUsername == null) {
            Random r = new Random();
            switch(r.nextInt(4)){
                case 0:
                    mUsername = names.get(r.nextInt(names.size()));
                    break;
                case 1:
                    mUsername = names.get(r.nextInt(names.size())) + names.get(r.nextInt(names.size()));
                    break;
                default:
                    mUsername = names.get(r.nextInt(names.size())) + r.nextInt(1000);
                    break;
            }
            // Assign a random user name if we don't have one saved.
            prefs.edit().putString("username", mUsername).commit();
        }
    }

    private void sendMessage() {
        EditText inputText = (EditText) findViewById(R.id.messageInput);
        String input = inputText.getText().toString();
        if (!input.trim().equals("")) {
            // Create our 'model', a Chat object
            Chat chat = new Chat(input, mUsername);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(chat);
            inputText.setText("");
        }
    }
    private void systemBroadcast(String message) {
        Chat chat = new Chat(mUsername + " " + message, "");
        // Create a new, auto-generated child of that chat location, and save our chat data there
        mFirebaseRef.push().setValue(chat);
    }
}
