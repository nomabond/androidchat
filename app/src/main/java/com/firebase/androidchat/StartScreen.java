package com.firebase.androidchat;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.regex.Pattern;

public class StartScreen extends Activity {

    Button newRoom, joinRoom;
    EditText joinRoomInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        newRoom = (Button) findViewById(R.id.newRoomButton);
        joinRoom = (Button) findViewById(R.id.joinRoomButton);
        joinRoomInput = (EditText) findViewById(R.id.joinRoomInput);

        joinRoomInput.setVisibility(View.GONE);

        newRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent startChat = new Intent(StartScreen.this, MainActivity.class);
                startActivity(startChat);
            }
        });

        joinRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(joinRoomInput.getVisibility() != View.VISIBLE) {
                    joinRoomInput.setVisibility(View.VISIBLE);
                } else {
                    Intent joinChat = new Intent(StartScreen.this, MainActivity.class);
                    String roomID = joinRoomInput.getText().toString().toUpperCase();
                    roomID = roomID.replaceAll("\\P{L}", "");
                    joinChat.putExtra("roomID", roomID);
                    startActivity(joinChat);
                }
            }
        });

    }
}
