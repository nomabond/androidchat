package com.firebase.androidchat;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.firebase.client.Query;

/**
 * @author greg
 * @since 6/21/13
 *
 * This class is an example of how to use FirebaseListAdapter. It uses the <code>Chat</code> class to encapsulate the
 * data for each individual chat message
 */
public class ChatListAdapter extends FirebaseListAdapter<Chat> {

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String mUsername;

    public ChatListAdapter(Query ref, Activity activity, int layout, String mUsername) {
        super(ref, Chat.class, layout, activity);
        this.mUsername = mUsername;
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     *
     * @param view A view instance corresponding to the layout we passed to the constructor.
     * @param chat An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(View view, Chat chat) {
        // Map a Chat object to an entry in our listview
        TextView authorText = (TextView) view.findViewById(R.id.author);
        TextView message = (TextView) view.findViewById(R.id.message);

        if(chat.getAuthor().equals("")){
            message.setTextColor(Color.parseColor("#e91e63"));
            message.setText(chat.getMessage());
            message.setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
            return;
        }

        String author = chat.getAuthor();
        authorText.setText(author + ": ");
        // If the message was sent by this user, color it differently
        if (author != null && author.equals(mUsername)) {
            // My text = Pink User and Dark Gray Text
            authorText.setTextColor(Color.parseColor("#26a69a"));
            message.setTextColor(Color.parseColor("#eceff1"));
        } else {
            // Other's text = Purple User and Black Text
            authorText.setTextColor(Color.parseColor("#80cbc4"));
            message.setTextColor(Color.parseColor("#b0bec5"));
        }

        message.setText(chat.getMessage());
    }
}
